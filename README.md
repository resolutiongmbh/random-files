# Random Files for Testing

Contains exactly that. The files are available in the Downloads section.

## Generate new test files

`dd if=/dev/random of=random15M.bin bs=1m count=15`

Then upload in the "Downloads" section of this project
